function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component, Suspense } from "react";
import ReactDOM from "react-dom";
import { ChromePicker } from "react-color";
import rgbHex from "rgb-hex";
import $ from "jquery"; // const ChromePicker = React.lazy(() => import("react-color"))

export default class ColorPicker extends Component {
  constructor(props) {
    super(props);

    _defineProperty(this, "onMouseLeaveHandler", e => {
      const $target = $(e.target);
      const p = ReactDOM.findDOMNode(this.input);

      if ($(p).length > 0 && !$target.closest(p).length) {
        $(p).css("display", "none");
      }
    });

    _defineProperty(this, "onColor", color => {
      this.setState({
        color: `#${rgbHex(color.rgb.r, color.rgb.g, color.rgb.b, color.rgb.a)}`
      });
      this.props.onChoose(color);
    });

    _defineProperty(this, "onColorToggle", () => {
      const p = $(`#color-picker-chooser${this.color_picker_id}`);
      if (p.length === 0) return;
      const offset = p.offset();
      console.log(offset); // const top = 0// $( document ).height() - offset.top > 300 ? 30 : -240;

      const clone = $(ReactDOM.findDOMNode(this.input));
      p.append($(clone).css({
        top: 25,
        left: 0,
        display: "block",
        zIndex: 1000
      }));
    });

    this.state = {
      color: this.props.color || "#FFFFFFFF",
      isColorPicker: false
    };
  }

  componentWillMount() {
    window.color_picker_id = typeof window.color_picker_id !== "undefined" ? window.color_picker_id + 1 : 1;
    this.color_picker_id = window.color_picker_id;
    document.body.addEventListener("click", this.onMouseLeaveHandler);
  }

  componentWillUnmount() {
    document.body.removeEventListener("click", this.onMouseLeaveHandler);
  }

  render() {
    const picker = /*#__PURE__*/React.createElement("div", {
      style: {
        position: "absolute",
        display: "none"
      },
      ref: node => {
        this.input = node;
      }
    }, /*#__PURE__*/React.createElement(ChromePicker, {
      color: this.state.color,
      onChange: this.onColor
    }));
    return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement("div", {
      style: {
        padding: "5px",
        background: "#fff",
        borderRadius: "1px",
        boxShadow: "0 0 0 1px rgba(0,0,0,.1)",
        display: "inline-block",
        cursor: "pointer",
        position: "relative",
        marginRight: 10
      },
      className: "picker_form",
      id: `color-picker-chooser${this.color_picker_id}`
    }, /*#__PURE__*/React.createElement("div", {
      style: {
        width: "28px",
        height: "28px",
        borderRadius: "2px",
        backgroundColor: this.state.color
      },
      onClick: this.onColorToggle
    })), picker);
  }

}