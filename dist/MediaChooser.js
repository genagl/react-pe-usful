function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component } from "react";
import $ from "jquery";
import in_array from "in_array";
import { Tabs, Tab, Card, Button } from "@blueprintjs/core";
import { __ } from "react-pe-utilities";
import { adapter } from "react-pe-layouts";
import WPMediaLibrary from "./WPMediaLibrary";
import WPMediaUploader from "./WPMediaUploader";
export default class MediaChooser extends Component {
  constructor(props) {
    super(props);

    _defineProperty(this, "handleTabChange", navbarTabId => {
      this.setState({
        tab: navbarTabId
      });
    });

    _defineProperty(this, "onImageUrl", evt => {
      this.setState({
        url: evt.currentTarget.value
      });
      this.props.onChange(evt.currentTarget.value, -1, this.props.ID);
    });

    _defineProperty(this, "onUpload", media => {
      //media.url, media.id
      this.setState({
        url: media.url,
        id: media.id,
        tab: "url"
      });
      this.props.onChange(media.url, -1, media.id);
    });

    _defineProperty(this, "onClear", () => {
      this.setState({
        url: "",
        id: -1,
        name: ""
      });
      this.props.onChange("", -1, this.props.ID);
    });

    _defineProperty(this, "onImageChange", evt => {
      const _height = this.state.height ? this.state.height : 70;

      const file = evt.target.files[0];
      if (!file) return;
      if ($(`#${this.props.prefix}imagex`).length) $(`#${this.props.prefix}imagex`).detach();
      const elem = document.getElementById(`#${this.props.prefix}imagex`);

      if (elem) {
        elem.parentNode.removeChild(elem);
      }
      /**/


      const img = document.createElement("img");
      img.height = _height;
      img.id = `${this.props.prefix}imagex`;
      img.style = `height:${_height}px`;
      img.alt = "";
      img.file = file;
      img.files = evt.target.files;
      const reader = new FileReader();
      reader.g = this;

      reader.onload = function (aImg) {
        return function (e) {
          // console.log(e);
          // console.log(aImg.file);
          aImg.src = e.target.result;
          reader.g.setState({
            url: aImg.src,
            name: aImg.file.name,
            file: aImg.file
          });
          reader.g.props.onChange(aImg.src, aImg.file, reader.g.props.ID);
        };
      }(img);

      reader.readAsDataURL(file); //
    });

    _defineProperty(this, "onLibrary", data => {
      this.setState({
        id: data.id,
        url: data.large
      }); // if(this.props.onChange)

      this.props.onChange(data.large, -1, data.id);
    });

    this.state = {
      id: props.id,
      // name: props.name,
      prefix: props.prefix,
      url: props.url,
      tab: typeof props.url == "string" && props.url.indexOf("data:image/") == 0 && !props.isUploadHide ? "ld" : "url"
    };
  }

  componentWillReceiveProps(nextProps) {
    const state = {
      id: nextProps.id,
      prefix: nextProps.prefix
    };

    if (nextProps.name) {
      state.name = nextProps.name;
    }

    if (typeof nextProps.url == "string") {
      state.url = nextProps.url;
    }

    this.setState(state);
  }

  componentDidUpdate() {
    const elem = document.getElementById(`${this.props.prefix}imagex`);
    if (elem) elem.setAttribute("src", this.state.url);
  }

  render() {
    const {
      isURLHide,
      isUploadHide
    } = this.props; // return this.form1();

    return /*#__PURE__*/React.createElement(Card, {
      className: "w-100"
    }, /*#__PURE__*/React.createElement(Tabs, {
      id: "TabsExample",
      onChange: this.handleTabChange,
      selectedTabId: this.state.tab
    }, isUploadHide ? null : /*#__PURE__*/React.createElement(Tab, {
      id: "ld",
      title: __("Upload"),
      panel: this.form1()
    }), isURLHide ? null : /*#__PURE__*/React.createElement(Tab, {
      id: "url",
      title: __("URL"),
      panel: this.formUrl()
    })));
  }

  formUrl() {
    const {
      height,
      padding
    } = this.props;
    const {
      id,
      prefix,
      url
    } = this.state;

    const _height = height || 70;

    const _padding = padding ? padding : 20;

    let image = "";

    if (url) {
      if (typeof url == "string" && url.indexOf("data:image/") != 0) {
        image = url;
      }
    }

    const bstyle = {
      height: _height + _padding * 2,
      margin: "3px 3px 3px 0",
      width: _height + _padding * 2,
      minWidth: _height + _padding * 2,
      backgroundColor: this.props.bg,
      padding: _padding,
      backgroundImage: `url(${image})`,
      backgroundSize: "cover",
      backgroundPosition: "center",
      boxShadow: "none"
    };
    return /*#__PURE__*/React.createElement("div", {
      style: {
        height: _height + 16
      },
      className: "d-flex"
    }, /*#__PURE__*/React.createElement("div", {
      className: "media_button my_image_upload",
      style: bstyle,
      image_id: id,
      prefix: prefix
    }), /*#__PURE__*/React.createElement("div", {
      className: "d-flex w-100 align-items-start"
    }, /*#__PURE__*/React.createElement("div", {
      className: "w-100"
    }, /*#__PURE__*/React.createElement("input", {
      className: "input dark form-control ml-2",
      value: image,
      onChange: this.onImageUrl
    }), adapter() == "wp" ? /*#__PURE__*/React.createElement(WPMediaLibrary, {
      onLibrary: this.onLibrary
    }) : null), /*#__PURE__*/React.createElement(Button, {
      className: "right",
      icon: "cross",
      minimal: true,
      onClick: this.onClear
    })));
  }

  form1() {
    const {
      id,
      prefix,
      url,
      bg
    } = this.state;
    let ext;
    let image; // const ext =  url && typeof url == "string" ? url.substring( url.lastIndexOf(".") + 1 ) : "";

    if (url) {
      if (typeof url == "string" && url.indexOf("data:image/") == 0) {
        ext = url.substring(url.lastIndexOf(".") + 1);
        image = url;
      } else if (typeof url == "object") {} else {
        ext = "";
        image = "";
      }
    } else {
      ext = "";
      image = "";
    }

    const {
      height,
      padding
    } = this.props;

    const _height = height || 70;

    const _padding = padding ? padding : 20;

    const bstyle = {
      height: _height + _padding * 2,
      margin: "3px 3px 3px 0",
      minWidth: _height + _padding * 2,
      backgroundColor: this.props.bg,
      padding: _padding
    }; // console.log(bstyle);

    const istyle = {
      position: "relative",
      display: "inline-flex",
      justifyContent: "center",
      alignItems: "center",
      minWidth: _height,
      height: _height,
      overflow: "hidden"
    };
    const delbtn = url != "" && url != undefined ? /*#__PURE__*/React.createElement("div", {
      className: "btn btn-link",
      style: {
        alignSelf: "start",
        padding: "3px 6px",
        marginTop: 4,
        lineHeight: "3px"
      },
      onClick: this.onClear
    }, /*#__PURE__*/React.createElement("i", {
      className: "fas fa-times"
    })) : null;
    const cont = in_array(ext, ["jpg", "gif", "svg", "png", "bmp"]) || (typeof url == "string" ? url.toString().indexOf("data:image/") != -1 : false) ?
    /*#__PURE__*/
    // <img
    // 	height={_height}
    // 	id={`${this.props.prefix}imagex`}
    // 	src={image}
    // 	alt=""
    // 	style={{ height: _height }}
    // />
    React.createElement("div", {
      style: {
        height: _height,
        width: _height,
        backgroundSize: "cover",
        backgroundPosition: "center",
        backgroundImage: "url(" + image + ")"
      }
    }) : /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement("div", {
      className: `fi fi-${ext} fi-size-xs`
    }, /*#__PURE__*/React.createElement("div", {
      className: "fi-content"
    }, ext)));
    const descr = this.props.isDescr || true ? /*#__PURE__*/React.createElement("span", {
      className: "media-chooser-descr"
    }, this.state.name) : null;
    return /*#__PURE__*/React.createElement("div", {
      className: "media-chooser-cont",
      style: {
        display: "flex",
        flexDirection: "row"
      }
    }, /*#__PURE__*/React.createElement("div", {
      className: "media_button my_image_upload",
      style: bstyle,
      image_id: id,
      prefix: prefix
    }, /*#__PURE__*/React.createElement("div", {
      className: "pictogramm ",
      id: prefix + id.toString(),
      style: istyle
    }, url ? cont : null, /*#__PURE__*/React.createElement("input", {
      type: "file",
      name: "image_input_name",
      style: {
        opacity: 0,
        width: "100%",
        height: "100%",
        position: "absolute",
        top: 0,
        left: 0
      },
      onChange: this.onImageChange
    }))), /*#__PURE__*/React.createElement("div", {
      className: "media-chooser-ext"
    }, delbtn, descr, adapter() == "wp" ? /*#__PURE__*/React.createElement(WPMediaUploader, {
      url: this.state.url,
      name: this.state.name,
      file: this.state.file,
      onUpload: this.onUpload
    }) : null));
  }

}