function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component } from "react";
import { Button, Dialog } from "@blueprintjs/core";
import { __ } from "react-pe-utilities";
import Loading from "./Loading";
import { withApollo } from "react-apollo";
import compose from "recompose/compose";
import gql from "graphql-tag";

class WPMediaLibrary extends Component {
  constructor(props) {
    super(props);

    _defineProperty(this, "onOpen", () => {
      this.setState({
        isOpen: !this.state.isOpen,
        loading: !this.state.isOpen
      });

      if (!this.state.isOpen) {
        const query = gql` query getMedias{ getMedias(paging:{ count:200, offset: 0 }){ id url mime large} }`;
        this.props.client.query({
          query
        }).then(result => {
          //console.log(result)	
          this.setState({
            loading: false,
            thumbnails: result.data.getMedias
          });
        });
      }
    });

    _defineProperty(this, "onThumbSelect", (thumb, i) => {
      //console.log(thumb, i)
      this.setState({
        select: this.state.select.id !== thumb.id ? thumb : {}
      });
    });

    _defineProperty(this, "getMedias", () => {
      const medias = this.state.thumbnails.map((thumb, i) => {
        return thumb.url !== "false" ? /*#__PURE__*/React.createElement("div", {
          key: i,
          className: "wp-thumbnail " + (thumb.id == this.state.select.id ? " active " : ""),
          style: {
            backgroundImage: "url(" + thumb.url + ")"
          },
          onClick: () => this.onThumbSelect(thumb, i)
        }) : null;
      });
      return medias;
    });

    _defineProperty(this, "onLibrary", () => {
      if (!this.state.select.id) return;

      if (this.props.onLibrary) {
        this.props.onLibrary(this.state.select);
      }

      this.setState({
        isOpen: false
      });
    });

    this.state = {
      isOpen: false,
      loading: false,
      thumbnails: [],
      select: {}
    };
  }

  componentDidMount() {}

  render() {
    return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(Button, {
      className: " ml-2 mt-3",
      onClick: this.onOpen
    }, __("Media library")), /*#__PURE__*/React.createElement(Dialog, {
      isOpen: this.state.isOpen,
      onClose: this.onOpen,
      title: __("Select media file"),
      className: "layout-outer-container  "
    }, /*#__PURE__*/React.createElement("div", {
      className: "d-flex flex-column",
      style: {
        height: "calc(100% - 50px)"
      }
    }, /*#__PURE__*/React.createElement("div", {
      className: "d-flex overflow-y-auto flex-grow-100 "
    }, this.state.loading ? /*#__PURE__*/React.createElement(Loading, null) : /*#__PURE__*/React.createElement("div", {
      className: "p-4 d-flex flex-wrap"
    }, this.getMedias())), /*#__PURE__*/React.createElement("div", {
      className: "d-flex flex-grow-1 justify-content-center align-items-center ",
      style: {
        height: 50
      }
    }, /*#__PURE__*/React.createElement(Button, {
      onClick: this.onLibrary
    }, __("Select")), /*#__PURE__*/React.createElement(Button, {
      icon: "cross",
      onClick: this.onOpen
    })))));
  }

}

export default compose(withApollo)(WPMediaLibrary);