function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component } from "react";
import { Button, Classes, Dialog, Intent, Position, Toaster } from "@blueprintjs/core";
import { __ } from "react-pe-utilities";
export const AppToaster = Toaster.create({
  position: Position.BOTTOM_RIGHT,
  maxToasts: 4
});
export class DeleteButton extends Component {
  constructor(props) {
    super(props);

    _defineProperty(this, "onRemoveDialog", () => {
      this.setState({
        isRemoveDialog: !this.state.isRemoveDialog
      });
    });

    _defineProperty(this, "onRemove", () => {
      this.setState({
        isRemoveDialog: !this.state.isRemoveDialog
      });
      this.props.onRelease();
    });

    this.state = {
      isRemoveDialog: false
    };
  }

  render() {
    return this.props.isDisabled ? /*#__PURE__*/React.createElement("span", null) : /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement("div", {
      className: "btn bg-danger tx-wt",
      onClick: this.onRemoveDialog
    }, /*#__PURE__*/React.createElement("i", {
      className: "fas fa-trash-alt mr-2"
    }), " ", __("Delete")), /*#__PURE__*/React.createElement(Dialog, {
      icon: "trash",
      isOpen: this.state.isRemoveDialog,
      onClose: this.onRemoveDialog,
      title: `${__("Do you realy want delete ") + this.props.post_title}?`
    }, /*#__PURE__*/React.createElement("div", {
      className: "pt-dialog-footer"
    }, /*#__PURE__*/React.createElement("div", {
      className: Classes.DIALOG_BODY
    }), /*#__PURE__*/React.createElement("div", {
      className: Classes.DIALOG_FOOTER
    }, /*#__PURE__*/React.createElement("div", {
      className: Classes.DIALOG_FOOTER_ACTIONS
    }, /*#__PURE__*/React.createElement(Button, {
      onClick: this.onRemoveDialog,
      text: __("oh, No! Sorry")
    }), /*#__PURE__*/React.createElement(Button, {
      intent: Intent.PRIMARY,
      onClick: this.onRemove,
      text: __("Yes, I want finaly delete this")
    }))))));
  }

}