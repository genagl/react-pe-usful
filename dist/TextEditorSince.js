function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component } from "react";
import { CKEditor } from "@ckeditor/ckeditor5-react";
import ClassicEditor from "@ckeditor/ckeditor5-build-classic";

class TextEditorSince extends Component {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "state", {
      text: this.props.text || ""
    });

    _defineProperty(this, "onReady", editor => {
      // You can store the "editor" and use when it is needed.
      console.log('Editor is ready to use!', editor);
    });

    _defineProperty(this, "onInit", () => {});

    _defineProperty(this, "onChange", (event, editor) => {
      const data = editor.getData();
      this.props.onChange(data);
      console.log({
        event,
        editor,
        data
      });
    });

    _defineProperty(this, "onBlur", (event, editor) => {
      console.log("Blur.", editor);
    });

    _defineProperty(this, "onFocus", (event, editor) => {
      console.log("Focus.", editor);
    });
  }

  render() {
    const {
      text
    } = this.state;
    return /*#__PURE__*/React.createElement("div", {
      className: "w-100"
    }, /*#__PURE__*/React.createElement(CKEditor, {
      editor: ClassicEditor,
      config: {
        height: 100,
        // language: 'ru',
        toolbar: {
          items: ['heading', '|', 'fontFamily', 'fontSize', 'fontColor', 'fontBackgroundColor', '|', // 'alignment', '|', 
          'bold', 'italic', 'strikethrough', 'underline', 'subscript', 'superscript', 'code', '|', 'link', '|', 'outdent', 'indent', '|', 'bulletedList', 'numberedList', 'todoList', '|', //'code', 'codeBlock', '|',
          'insertTable', '|', 'blockQuote', '|', 'undo', 'redo', '|'],
          shouldNotGroupWhenFull: true
        }
      },
      data: text,
      onChange: this.onChange,
      onBlur: this.onBlur,
      onFocus: this.onFocus,
      onReady: this.onReady,
      rows: 10
    }));
  }

}

export default TextEditorSince;
/* https://ckeditor.com/docs/ckeditor5/latest/builds/guides/integration/frameworks/react.html */