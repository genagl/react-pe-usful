import React, { Component } from "react";
export default class Loading extends Component {
  render() {
    return /*#__PURE__*/React.createElement("div", {
      className: "loader-cont"
    }, /*#__PURE__*/React.createElement("div", {
      className: "loader fa-spin " + this.props.classNames
    }));
  }

}