import { Callout, Card } from "@blueprintjs/core";
import React, { useState } from "react";
import { __ } from "react-pe-utilities";
import { Loading } from "react-pe-useful";
import app from "react-pe-layouts";
import { server_url__, gitlab_private_token } from "react-pe-layouts";

const Issue = props => {
  const [loading, changeLoading] = useState(false);
  const [title, onTitle] = useState("");
  const [descr, onDescr] = useState("");
  const [name, onName] = useState("");
  const [anwer, onAnswer] = useState("");

  const loadList = () => {
    fetch("https://gitlab.com/api/v4/projects/22486508/issues", {
      method: 'GET',
      // или 'PUT'
      //body: JSON.stringify(data), // данные могут быть 'строкой' или {объектом}!
      headers: {
        'Content-Type': 'application/json',
        'PRIVATE-TOKEN': gitlab_private_token()
      }
    }).then(response => {
      console.log(response);
      changeLoading(false);
    });
  };

  const sendIssue = () => {
    const data = {
      title,
      description: descr + `. 
                From: [**${app.title}**](${server_url__()}). 
                Publisher: **${name ? name : "unknown"}** 
                distination: **${props.src}**
                sectionID: **${props.sectionID}**`,
      issue_type: "test_case"
    };
    console.log(data);
    changeLoading(true);
    fetch("https://gitlab.com/api/v4/projects/22486508/issues", {
      method: 'POST',
      // или 'PUT'
      body: JSON.stringify(data),
      // данные могут быть 'строкой' или {объектом}!
      headers: {
        'Content-Type': 'application/json',
        'PRIVATE-TOKEN': gitlab_private_token()
      }
    }).then(response => {
      console.log(response);
      changeLoading(false);
      onTitle("");
      onDescr("");
      onName("");
      onAnswer(__("The developers are grateful to you for your attention to our development. In the near future, the problems will be fixed, and your suggestions for improvement will be taken into account. Thanks."));
    });
  }; // loadList()


  const setTitle = evt => {
    onTitle(evt.currentTarget.value);
  };

  const setDescr = evt => {
    onDescr(evt.currentTarget.value);
  };

  const setName = evt => {
    onName(evt.currentTarget.value);
  }; // console.log( name, title , descr) 


  return loading ? /*#__PURE__*/React.createElement("div", {
    className: "w-100 h-100 "
  }, /*#__PURE__*/React.createElement(Loading, null)) : anwer ? /*#__PURE__*/React.createElement(Callout, {
    className: "p-5"
  }, anwer) : /*#__PURE__*/React.createElement("div", {
    className: "w-100 h-100 d-flex align-items-center justify-content-center lead"
  }, /*#__PURE__*/React.createElement(Card, {
    className: "w-100 p-5 "
  }, /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement("label", null, __("Insert title")), /*#__PURE__*/React.createElement("input", {
    type: "text",
    className: "form-control input dark",
    value: title,
    onChange: setTitle
  })), /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement("label", null, __("Insert description")), /*#__PURE__*/React.createElement("textarea", {
    rows: 6,
    className: "form-control ",
    onChange: setDescr,
    value: descr
  }, descr)), /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement("label", null, __("Insert your name please")), /*#__PURE__*/React.createElement("input", {
    type: "text",
    className: "form-control input dark",
    value: name,
    onChange: setName
  })), /*#__PURE__*/React.createElement("div", {
    className: "card-footer d-flex"
  }, /*#__PURE__*/React.createElement("div", {
    className: "btn btn-sm btn-primary m-1",
    onClick: sendIssue
  }, __("Send new issue")), /*#__PURE__*/React.createElement("a", {
    target: "_blank",
    href: "https://gitlab.com/protopiahome/inner-projects/oraculi/unstable/oraculi-react/-/issues",
    className: "m-2 btn btn-sm"
  }, __("View all issues")))));
};

export default Issue;