import React, { useEffect } from "react"
import Editor from "react-simple-code-editor";
import Prism  from "prismjs" 
import "prismjs/components/prism-clike";
import "prismjs/components/prism-javascript";
import "prismjs/themes/prism.css"; //Example style, you can use another
import $ from "jquery"


const CodeEditor = props =>
{
    const [code, setCode] = React.useState( props.value );
    useEffect(() =>
    {
        if(code !== props.value)
        {
            setCode(props.value)
        }
    })
    const onEdit = code =>
    {
        setCode(code)
        if(props.onChange)
        {
            props.onChange(code)
        }
    }
    const loadFonts = () =>
    {
        if($("head link#monoFont").length == 0)
        {
            $("head").append(`<link id="monoFont" href="https://fonts.googleapis.com/css2?family=Fira+Mono&display=swap" rel="stylesheet">`)
        }
    }
    useEffect(() => {
        Prism.manual = true;
        Prism.highlightAll();
        loadFonts()
    }, [])
    return (
        <Editor
            value={code}
            onValueChange={ onEdit }
            highlight={(code) =>  Prism.highlight(code, Prism.languages.html, 'html')}
            padding={10}
            style={{
                fontFamily: '"Fira code", "Fira Mono", monospace',
                fontSize: 12,
                color: "#111!important",
                textFillColor: "#111!important"
            }}
        />
    );
}

export default CodeEditor