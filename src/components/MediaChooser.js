import React, { Component } from "react"
import $ from "jquery"
import in_array from "in_array"
import { Tabs, Tab, Card, Button } from "@blueprintjs/core"
import { __ } from "react-pe-utilities"
import { adapter } from "react-pe-layouts"
import WPMediaLibrary from "./WPMediaLibrary"
import WPMediaUploader from "./WPMediaUploader"

export default class MediaChooser extends Component {
	constructor(props) {
		super(props)
		this.state = {
			id: props.id,
			// name: props.name,
			prefix: props.prefix,
			url: props.url,
			tab: typeof props.url == "string" && props.url.indexOf("data:image/") == 0 && !props.isUploadHide ? "ld" : "url",
		}
	}
	componentWillReceiveProps(nextProps) {
		const state = {
			id: nextProps.id,
			prefix: nextProps.prefix,
		}
		if (nextProps.name) {
			state.name = nextProps.name
		}
		if (typeof nextProps.url == "string") {
			state.url = nextProps.url
		}
		this.setState( state )
	}

	componentDidUpdate() {
		const elem = document.getElementById(`${this.props.prefix}imagex`)
		if (elem) elem.setAttribute("src", this.state.url)
	}

	render() {
		const {isURLHide, isUploadHide} = this.props
		// return this.form1();
		return (
			<Card className="w-100">
				<Tabs id="TabsExample" onChange={this.handleTabChange} selectedTabId={this.state.tab}>
					{
						isUploadHide
							?
							null
							:						
							<Tab
								id="ld"
								title={__("Upload")}
								panel={
									this.form1()
								}
							/>
					}
					{
						isURLHide
							?
							null
							:
							<Tab
								id="url"
								title={__("URL")}
								panel={
									this.formUrl()
								}
							/>	
					}
					
				</Tabs>
			</Card>
		)
	}

	handleTabChange = (navbarTabId) => {
		this.setState({ tab: navbarTabId })
	}

	formUrl() {
		const { height, padding } = this.props
		const { id, prefix, url } = this.state
		const _height = height || 70
		const _padding = padding ? padding : 20
		let image = ""
		if (url) {
			if (typeof url == "string" && url.indexOf("data:image/") != 0) {
				image = url
			}
		}
		const bstyle = {
			height: _height + _padding * 2,
			margin: "3px 3px 3px 0",
			width: _height + _padding * 2,
			minWidth: _height + _padding * 2,
			backgroundColor: this.props.bg,
			padding: _padding,
			backgroundImage: `url(${image})`,
			backgroundSize: "cover",
			backgroundPosition:"center",
			boxShadow: "none",
		}

		return (
			<div
				style={{ height: _height + 16 }}
				className="d-flex"
			>
				<div className="media_button my_image_upload" style={bstyle} image_id={id} prefix={prefix} />
				<div className="d-flex w-100 align-items-start">
					<div className="w-100">
						<input className="input dark form-control ml-2" value={image} onChange={this.onImageUrl} />
						{ 
							adapter() == "wp" 
								?
								<WPMediaLibrary
									onLibrary={this.onLibrary}
								/>
								:
								null
						 }
					</div>
					<Button
						className="right"
						icon="cross"
						minimal
						onClick={this.onClear}
					/>

				</div>
			</div>
		)
	}
	onImageUrl = (evt) => {
		this.setState({ url: evt.currentTarget.value })
		this.props.onChange(evt.currentTarget.value, -1, this.props.ID)
	}

	form1() {
		const {
			id, prefix, url, bg,
		} = this.state
		let ext
		let image
		// const ext =  url && typeof url == "string" ? url.substring( url.lastIndexOf(".") + 1 ) : "";
		if (url) 
		{
			if (typeof url == "string" && url.indexOf("data:image/") == 0) 
			{
				ext = url.substring(url.lastIndexOf(".") + 1)
				image = url
			} 
			else if (typeof url == "object") 
			{

			}
			else 
			{
				ext = ""
				image = ""
			}
		} 
		else 
		{
			ext = ""
			image = ""
		}
		const { height, padding } = this.props
		const _height = height || 70
		const _padding = padding ? padding : 20
		const bstyle = {
			height: _height + _padding * 2,
			margin: "3px 3px 3px 0",
			minWidth: _height + _padding * 2,
			backgroundColor: this.props.bg,
			padding: _padding,
		}
		// console.log(bstyle);
		const istyle = {
			position: "relative",
			display: "inline-flex",
			justifyContent: "center",
			alignItems: "center",
			minWidth: _height,
			height: _height,
			overflow: "hidden",
		}
		const delbtn = url != "" && url != undefined ? (
			<div
				className="btn btn-link"
				style={{
					alignSelf: "start", padding: "3px 6px", marginTop: 4, lineHeight: "3px",
				}}
				onClick={this.onClear}
			>
				<i className="fas fa-times" />
			</div>
		) : null

		const cont = in_array(
				ext, 
				["jpg", "gif", "svg", "png", "bmp"])
				|| 
				(typeof url == "string" ? url.toString().indexOf("data:image/") != -1 : false
			)
			? (
				// <img
				// 	height={_height}
				// 	id={`${this.props.prefix}imagex`}
				// 	src={image}
				// 	alt=""
				// 	style={{ height: _height }}
				// />
				<div
					style={{
						height: _height,
						width: _height,
						backgroundSize:"cover",
						backgroundPosition:"center",
						backgroundImage:"url(" + image + ")"
					}}
				>

				</div>
			)
			: (
				<div>
					<div className={`fi fi-${ext} fi-size-xs`}>
						<div className="fi-content">{ext}</div>
					</div>
				</div>
			)
		const descr = this.props.isDescr || true
			? (
				<span className="media-chooser-descr">
					{this.state.name}
				</span>
			)
			: null
		return (
			<div className="media-chooser-cont" style={{ display: "flex", flexDirection: "row" }}>
				<div className="media_button my_image_upload" style={bstyle} image_id={id} prefix={prefix}>
					<div className="pictogramm " id={prefix + id.toString()} style={istyle}>
						{url ? cont : null}
						<input
							type="file"
							name="image_input_name"
							style={{
								opacity: 0, 
								width: "100%", 
								height: "100%", 
								position: "absolute", 
								top: 0, 
								left: 0,
							}}
							onChange={this.onImageChange}
						/>
					</div>
				</div>
				<div className="media-chooser-ext">
					{delbtn}
					{descr}
					{ 
					adapter() == "wp" 
						?
						<WPMediaUploader
							url={this.state.url}
							name={this.state.name}
							file={this.state.file} 
							onUpload={this.onUpload}
						/>
						:
						null
					}
				</div>
			</div>
		)
	}
	onUpload = media =>
	{
		//media.url, media.id
		this.setState({
			url : media.url,
			id	: media.id,
			tab:"url"
		})
		this.props.onChange(media.url, -1, media.id)
	}
	onClear = () => {
		this.setState({ url: "", id: -1, name: "" })
		this.props.onChange("", -1, this.props.ID)
	}

	onImageChange = (evt) => {
		const _height = this.state.height ? this.state.height : 70
		const file = evt.target.files[0]
		if (!file) return
		if ($(`#${this.props.prefix}imagex`).length) $(`#${this.props.prefix}imagex`).detach()
		const elem = document.getElementById(`#${this.props.prefix}imagex`)
		if (elem) {
			elem.parentNode.removeChild(elem)
		}
		/**/
		const img = document.createElement("img")
		img.height = _height
		img.id = `${this.props.prefix}imagex`
		img.style = `height:${_height}px`
		img.alt = ""
		img.file = file
		img.files = evt.target.files
		const reader = new FileReader()
		reader.g = this

		reader.onload = (function (aImg) {
			return function (e) {
				// console.log(e);
				// console.log(aImg.file);

				aImg.src = e.target.result
				reader.g.setState({ url: aImg.src, name: aImg.file.name, file: aImg.file})
				reader.g.props.onChange(aImg.src, aImg.file, reader.g.props.ID)
			}
		}(img))
		reader.readAsDataURL(file)

		//
	}
	onLibrary = data =>
	{
		this.setState({
			id: data.id,
			url: data.large
		})
		// if(this.props.onChange)
			this.props.onChange( data.large, -1, data.id)
	}
}
