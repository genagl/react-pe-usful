import React, { Component } from "react"

export default class Loading extends Component {
  render() {
    return (
      <div className="loader-cont">
        <div className={"loader fa-spin " + this.props.classNames } />
      </div>
    ) 
  }
}
